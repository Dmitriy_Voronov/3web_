<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title> Задание 3 (Главная страница) </title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="style.css"/>
</head>
 <body>
 <div class="nav">
    <form id="start" action="" method="post">
		<label> Введите имя 
            <br>
            <input name=fio type = "text" placeholder = "ФИО" required> </label>
		<br>
		<br>
		<label> Введите почту 
			<br>
			<input name="email" type = "email" placeholder = "Почта" required> </label>
		<br>
		<br>
		<label> Введите дату рождения 
			<br>
			<input name="data" type = "date" required> </label>
		<br>
		<br>
		<div> Выберите пол
			<br> 
			<label>
				<input type = "radio" value = "m" name = "pol" required> муж </label>
			<label>
				<input type = "radio" value = "f" name = "pol" > жен </label>
		</div>
		<br>
		<div> Количество конечностей
			<br>
			<label>
				<input type = "radio" value = "1" name = "Limbs" required> 1
			</label>
			<label>
				<input type = "radio" value = "2" name = "Limbs" > 2
			</label>
			<label>
				<input type = "radio" value = "3" name = "Limbs" > 3
			</label>
			<label>
				<input type = "radio" value = "4" name = "Limbs" > 4
			</label>
		</div>
		<br>
		<div> Сверхспособности:
			<br>
			<select name="abilities[]" multiple = "multiple" required>
				<option value = "Значение 1"> Бессмертие </option>
				<option value = "Значение 2"> Прохождение сквозь стены </option>
				<option value = "Значение 3"> Левитация </option>
			</select>
			<br>
		</div>
		<br>
		Биография
		<br>
		<textarea name = "Biografia" cols = "40" rows = "10" required> </textarea>
		<br>
		<br>
		<label>
			<input type = "checkbox" name = "Контракт" required> С контрактом ознакомлен
		</label>
		<br>
		<br>
		<input type = "submit" name="done" value = "Отправить"/>
	</form>
	</div>
</body>
</html> 
